Purpose
-------
The Mails-IN (MIN) project utilizes two remote base stations, including a master and slave, that notifies the user when the mail has arrived.

Information
-----------
Austin Schaller     
schaller.austin@gmail.com