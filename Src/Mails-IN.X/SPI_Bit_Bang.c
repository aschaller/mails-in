/*******************************************************************************
 * Copyright (c) 2012 Austin R. Schaller
 * <schaller.austin@gmail.com>
 *
 * Module:          SPI_Bit_Bang.c
 * Description:     SPI bit bang library file.
 *
*******************************************************************************/


/*******************************************************************************
 * PLEASE NOTE
 * The following parameters must be predefined in your main() file:
 *
 *      1. SCL_T - Clock Latch TRIS (e.g. #define SCL_T TRISAbits.TRISA0)
 *      2. SCL - Clock Latch (e.g. #define SCL PORTAbits.RA0)
 *      3. SDO_T - Data Out TRIS (e.g. #define SDO_T TRISAbits.TRISA1)
 *      4. SDO - Data Out (e.g. #define SDO PORTAbits.RA1)
 *      5. SDI_T Data In TRIS(e.g. #define SDI_T TRISAbits.TRISA2)
 *      6. SDI - Data In (e.g. #define SDI PORTAbits.RA2)
 *      7. CS_T Chip Select TRIS (e.g. #defie CS_T TRISAbits.TRISA3)
 *      8. CS - Chip Select (e.g. #define CS PORTAbits.RA3)
 *      9. F_OSC - Frequency of PIC oscillator (e.g. #define F_OSC 8000000)
 *
*******************************************************************************/



/*******************************************************************************
 *
 * Purpose:         Initializes SPI I/O pins for operation.
 * Passed:          Void
 * Returned:        Void
 *
*******************************************************************************/
void InitSPIBitBang(void)
{
    SCL_T = 0;
    SDO_T = 0;
    SDI_T = 0;
    SS_T = 0;

    SS = 0;
    SDO = 0;
    SCL = 0;
}

/*******************************************************************************
 *
 * Purpose:         Write a single byte from the Master to the Slave device.
 * Passed:          The byte to be sent.
 * Returned:        Void
 *
*******************************************************************************/
unsigned char WriteByteSPI(unsigned char data)
{
    unsigned char index;

    for(index = 0; index < 8; index++)
    {
        // SS active high on trailing edge of previous clock
        SCL = 0;
        SDO = 0;

        if((data & 0x80) != 0)
            SDO = 1;

        // Delay
        SCL = 1;
        // Delay

        data <<= 1;
    }
}

/*******************************************************************************
 *
 * Purpose:         Write the standard address and data bytes from the Master
 *                  to the Slave device.
 * Passed:          The address byte of the Slave and data to be sent.
 * Returned:        Void
 *
*******************************************************************************/
unsigned char WriteSPI(unsigned char address, unsigned char data)
{
    SCL = 1;
    CS = 1;     // Start TX
    WriteByteSPI(address);
    WriteByteSPI(data);
    CS = 0;     // End TX
}

/*******************************************************************************
 *
 * Purpose:         Read a standard data byte from the Slave device.
 * Passed:          Void
 * Returned:        The byte received from the Slave device.
 *
*******************************************************************************/
unsigned char ReadByteSPI(void)
{
    unsigned char index, data;
    
    for(index = 0; index < 8; index++)
    {
        SCL = 0;
        // Delay
        
        data <<= 1;
        data &= 0xFE;       // Clear everything except LSB
        
        if(SDI)
            data |= 1;
        
        // Delay
        SCL = 1;
    }

    return data;
}

/*******************************************************************************
 *
 * Purpose:         Read the standard address and data bytes from the slave
 *                  device.
 * Passed:          The address byte of the Slave.
 * Returned:        The byte received from the Slave device.
 *
*******************************************************************************/
unsigned char ReadSPI(unsigned char address)
{
    unsigned char data;

    SCL = 1;
    CS = 1;     // Start RX
    WriteByteSPI(address);
    data = ReadByteSPI();
    CS = 0;     // End RX

    return data;
}
